{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE OverloadedStrings #-}
module Language.Hawk.Analysis.Typecheck where

import Data.IORef.MonadIO

import Language.Hawk.Analysis.Typecheck.Monad
import Language.Hawk.Syntax.Concrete


-- -----------------------------------------------------------------------------
-- | Type Checking

typecheck :: Exp -> Tc PolyType
typecheck e = do 
  ty <- inferPoly e
  zonkType ty


-- -----------------------------------------------------------------------------
-- | Bidirectional Expectations

data Expected a = Infer (IORef a) | Check a


-- -----------------------------------------------------------------------------
-- | Rho Type Checking

checkRho :: Exp -> RhoType -> Tc Exp
checkRho e rho = tcRho e (Check rho)

inferRho :: Exp -> Tc (Exp, RhoType)
inferRho exp = do 
  ref <- newTcRef (error "inferRho: empty result")
  exp' <- tcRho exp (Infer ref)
  rho <- readTcRef ref
  return (exp', rho)

tcRho :: Exp -> Expected RhoType -> Tc Exp
tcRho exp expected_rho = case exp of
  EVar n -> do
    poly <- lookupVar (nameBase n)
    poly' <- instPoly  poly expected_rho
    return $ EType Nothing (EVar n) (TypeSig Nothing poly')



-- -----------------------------------------------------------------------------
-- | Poly Type Checking


inferPoly :: Exp -> Tc PolyType
inferPoly e = do
  return undefined


checkPoly :: Exp -> PolyType -> Tc ()
checkPoly e poly = undefined


-- -----------------------------------------------------------------------------
-- | Subsumption Checking

subsCheck :: PolyType -> PolyType -> Tc ()
subsCheck poly1 poly2 = do                  -- Rule DEEP-SKOL
  (skol_tvs, rho2) <- skolemise poly2
  subsCheckRho poly1 rho2
  esc_tvs <- getFreeTyVars [poly1, poly2]
  let bad_tvs = filter (`elem` esc_tvs) skol_tvs
  check (null bad_tvs)
        


subsCheckRho :: PolyType -> RhoType -> Tc ()
subsCheckRho t1 t2 = case (t1, t2) of
  (poly1@(TForall _ _ _), rho2) -> do       -- Rule SPEC
    rho1 <- instantiate poly1
    subsCheckRho rho1 rho2

  (rho1, TArr _ a2 r2) -> do                -- Rule FUN
    (a1, r1) <- unifyFun rho1
    subsCheckFun a1 r1 a2 r1
  
  (TArr _ a1 a2, rho2) -> do                -- Rule FUN
    (a2, r2) <- unifyFun rho2
    subsCheckFun a1 r1 a2 r2

  (tau1, tau2) ->                           -- Rule MONO
    unify tau1 tau2



subsCheckFun :: PolyType -> RhoType -> PolyType -> RhoType -> Tc ()
subsCheckFun a1 r1 a2 r2 = do
  subsCheck a2 a1
  subsCheckRho r1 r2
  

instPoly :: PolyType -> Expected RhoType -> Tc PolyType
-- Invariant: if the second argument is (Check rho),
-- 	          then rho is in weak-prenex form
instPoly t1 = \case
  Check t2 -> do
    subsCheckRho t1 t2
    return t2

  Infer r  -> do
    t1' <- instantiate t1
    writeTcRef r t1'
    return t1'