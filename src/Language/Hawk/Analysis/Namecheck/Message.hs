{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE OverloadedStrings #-}
module Language.Hawk.Analysis.Namecheck.Message where

import Data.Text (Text)
import Data.Text.Prettyprint.Doc hiding (group)
import Data.Text.Prettyprint.Doc.Render.Text (renderStrict)

import Language.Hawk.Syntax.Location

data NcMsg
  = NameNotFound Loc Text [Text]
  | NameConflict Text Loc Loc


instance Pretty NcMsg where
    pretty = \case
        NameNotFound l n suggs ->
            vsep [ pretty l <+> "Name Not Found:" <+> pretty n
                 , "Suggestions: " <+> hsep (pretty <$> suggs)
                 ]

        
        NameConflict n l l' ->
            vsep [ "Name conflict detected between"
                 , pretty n <+> "at" <+> pretty l'
                 , "and" <+> pretty n <+> "at" <+> pretty l
                 ]