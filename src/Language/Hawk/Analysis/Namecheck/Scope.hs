{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE TemplateHaskell #-}
module Language.Hawk.Analysis.Namecheck.Scope where

import Control.Lens
import Data.HashMap.Strict (HashMap)
import Data.Set (Set)
import Data.Text (Text)
import Language.Hawk.Syntax.Concrete

import qualified Data.Set as Set
import qualified Data.Text as T
import qualified Data.HashMap.Strict as HMap

type Table a = HashMap Text a
type ModuleTable = Table Module
type InterfaceTable = Table Interface

-- -----------------------------------------------------------------------------
-- | Global Scope

data GlobalScope
  = GlobalScope
    { _globalVarTable           :: Table Name
    , _globalTVarTable          :: Table Name
    , _globalConTable           :: Table Name
    , _globalTConTable          :: Table Name
    , _globalModulePaths        :: Set Text
    , _globalModuleTable        :: ModuleTable
    , _globalInterfacePaths     :: Set Text
    , _globalInterfaceTable     :: InterfaceTable
    }

instance Semigroup GlobalScope where
  (<>) (GlobalScope a1 b1 c1 d1 e1 f1 g1 h1)
       (GlobalScope a2 b2 c2 d2 e2 f2 g2 h2)
    = GlobalScope (a1<>a2) (b1<>b2) (c1<>c2)
                  (d1<>d2) (e1<>e2) (f1<>f2)
                  (g1<>g2) (h1<>h2)

instance Monoid GlobalScope where
  mempty = GlobalScope mempty mempty mempty
                       mempty mempty mempty
                       mempty mempty


-- -----------------------------------------------------------------------------
-- | Local Scope

data LocalScope
  = LocalScope { _localStack :: [LocalFrame] }

data LocalFrame
  = LocalFrame
    { _localVars        :: Set Text
    , _localTVars       :: Set Text
    }

instance Semigroup LocalScope where
  (<>) (LocalScope a1)
       (LocalScope a2)
    = LocalScope (a1<>a2)

instance Monoid LocalScope where
  mempty = LocalScope mempty


instance Semigroup LocalFrame where
  (<>) (LocalFrame a1 b1)
       (LocalFrame a2 b2)
    = LocalFrame (a1<>a2) (b1<>b2)

instance Monoid LocalFrame where
  mempty = LocalFrame mempty mempty

--searchLocalScope :: Text -> LocalScope -> [Text]
--searchLocalScope txt (LocalScope stack) = 

-- searchScopeVars

checkLocalVar :: Text -> LocalScope -> Bool
checkLocalVar v (LocalScope stack)
  = or $ do
      (LocalFrame vars _) <- stack
      return (Set.member v vars)


checkLocalTVar :: Text -> LocalScope -> Bool
checkLocalTVar v (LocalScope stack)
  = or $ do
      (LocalFrame _ tvars) <- stack
      return (Set.member v tvars)





-- -----------------------------------------------------------------------------
-- | Lenses

makeLenses ''LocalScope
makeLenses ''LocalFrame
makeLenses ''GlobalScope


-- -----------------------------------------------------------------------------
-- | Table Construction
 
buildInterfaceTable :: [IObject] -> InterfaceTable
buildInterfaceTable iobjs
  = HMap.fromList [ (unpackInterfacePath i, i) | i <- findInterfaces iobjs ]

buildModuleTable :: [Object] -> ModuleTable 
buildModuleTable objs
  = HMap.fromList [ (unpackModulePath m, m) | m <- concatMap findModules objs]


-- -----------------------------------------------------------------------------
-- | Scope Extraction

objectScope :: Object -> GlobalScope
objectScope obj = undefined

moduleScope :: Module -> GlobalScope
moduleScope m = undefined

iobjectScope :: IObject -> GlobalScope
iobjectScope obj = undefined

interfaceScope :: Interface -> GlobalScope
interfaceScope iface = undefined

statementScope :: Stmt -> LocalScope
statementScope stmt = undefined


-- -----------------------------------------------------------------------------
-- | Global Scope Helpers




-- -----------------------------------------------------------------------------
-- | Local Scope Helpers

