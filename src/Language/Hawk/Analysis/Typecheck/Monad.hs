{-# LANGUAGE ConstraintKinds #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell #-}
module Language.Hawk.Analysis.Typecheck.Monad where

import Control.Lens
import Control.Concurrent.Supply
import Control.Monad.Chronicle.Class
import Control.Monad.IO.Class
import Control.Monad.State
import Data.IORef.MonadIO
import Data.HashMap.Strict (HashMap)
import Data.IntMap.Strict  (IntMap)
import Data.Text  (Text, unpack)
import Data.These (These)

import Language.Hawk.Analysis.Typecheck.Message
import Language.Hawk.Analysis.Typecheck.Substitution (Subst, subst)
import Language.Hawk.Syntax.Concrete

import qualified Data.HashMap.Strict as HMap
import qualified Data.IntMap.Strict  as IMap 
import qualified Data.Text as T

import qualified Language.Hawk.Syntax.Concrete.Primitive as Prim
import qualified Language.Hawk.Analysis.Typecheck.Substitution as S

-- -----------------------------------------------------------------------------
-- | Typechecking Environment

data TcEnv 
  = TcEnv
    { _tcVarEnv    :: HashMap Text PolyType
    , _tcSupply    :: Supply
    }

makeLenses ''TcEnv

newTcEnv :: [(Name, PolyType)] -> IO TcEnv
newTcEnv vars = do
  let vars' = vars & mapped . _1 %~ nameBase
      var_env' = HMap.fromList vars'
  supply <- newSupply
  return $ TcEnv mempty supply


-- -----------------------------------------------------------------------------
-- | Typechecking Monad

-- | Our monad consists of an environment reader and a error chronicle for
-- reporting intelligent error messages.

type MonadTc m = (MonadState TcEnv m, MonadChronicle [TcMsg] m, MonadIO m)
type Tc a = StateT TcEnv (ChronicleT [TcMsg] IO) a

-- | Run typechecking monad, given a dictionary of known variable types.
runTc :: [(Name, PolyType)] -> Tc a -> IO (These [TcMsg] a)
runTc vars tc = do
  env <- newTcEnv vars
  runChronicleT (evalStateT tc env)


-- -----------------------------------------------------------------------------
-- | Variable Helpers

-- | Supply an integer to the environment
supplyInt :: Tc Int
supplyInt = do
  (i, supply') <- uses tcSupply freshId
  tcSupply .= supply'
  return i


-- | Variable Environment Extension
extendVarEnv :: Name -> PolyType -> Tc a -> Tc a
extendVarEnv n ty m = withStateT extend m
  where
    n_txt = T.pack $ show n
    extend (TcEnv var_env supply) = TcEnv (HMap.insert n_txt ty var_env) supply


lookupVar :: Text -> Tc PolyType
lookupVar n = do
  var_env <- use tcVarEnv
  case HMap.lookup n var_env of
    Nothing -> error ("Not in scope: " <> unpack n)
    Just ty -> return ty

-- -----------------------------------------------------------------------------
-- | Typecheck References Helpers

-- IORef helpers that handle lifting into Tc
newTcRef :: MonadTc m => a -> m (IORef a)
newTcRef = liftIO . newIORef

readTcRef :: MonadTc m => IORef a -> m a
readTcRef = liftIO . readIORef

writeTcRef :: MonadTc m => IORef a -> a -> m ()
writeTcRef r = liftIO . writeIORef r 


-- -----------------------------------------------------------------------------
-- | Meta Type Variable Helpers

newMetaTv :: Int -> Tc MetaTv
newMetaTv i = MetaTv i <$> newTcRef Nothing

readMetaTv :: MetaTv -> Tc (Maybe MonoType)
readMetaTv (MetaTv _ ref) = readTcRef ref

writeMetaTv :: MetaTv -> MonoType -> Tc ()
writeMetaTv (MetaTv _ ref) ty = writeTcRef ref (Just ty)


-- -----------------------------------------------------------------------------
-- | Type Variable Helpers

newTyVarTy :: Tc MonoType
newTyVarTy = 
  TMeta <$> newMetaTyVar

newMetaTyVar :: Tc MetaTv
newMetaTyVar =
  supplyInt >>= newMetaTv

newSkolemVar :: TVar -> Tc TVar
newSkolemVar tv = do
  i <- supplyInt 
  return $ TvSkolem Nothing (Just $ textTVar tv) i


-- -----------------------------------------------------------------------------
-- | Instantition

instantiate :: PolyType -> Tc RhoType
instantiate = \case
  TForall _ tvs ty -> do
    tvs' <- mapM (\_ -> newMetaTyVar) tvs
    return $ subst tvs (TMeta <$> tvs') ty
  
  ty -> return ty



skolemise :: PolyType -> Tc ([TVar], RhoType)
skolemise = \case
  TForall _ tvs ty -> do
    sks1 <- mapM newSkolemVar tvs
    (sks2, ty') <- skolemise (subst tvs (TVar <$> sks1) ty)
    return (sks1 <> sks2, ty')

  TArr l arg_ty ret_ty -> do
    (sks, ty') <- skolemise ret_ty    -- Rule PRFUN
    return (sks, TArr l arg_ty ty')

  ty ->
    return ([], ty)

-- -----------------------------------------------------------------------------
-- | Quantification

quantify :: [MetaTv] -> RhoType -> Tc PolyType
quantify tvs ty = do
    mapM_ bind (tvs `zip` new_bndrs)
    ty' <- zonkType ty
    return $ ForAll new_bndrs ty'

  where
    used_bndrs = tyVarBndrs ty
    new_bndrs = take (length tvs) (allBinders \\ used_bndrs)
    bind (tv, name) = writeTv tv (TyVar name)


allBinders :: [TyVar]
allBinders = [ BoundTv [x]    | x <- ['a' .. 'z'] ] ++
             [ BoundTv (x : show i) | i <- [1 :: Integer ..], x <- ['a'..'z'] ]


-- -----------------------------------------------------------------------------
-- | Zonking - erasing type variables

zonkType :: Type -> Tc Type
zonkType = \case
  TForall l ns ty ->
    Forall l ns <$> zonkType ty

  TVar n ->
    return $ TVar n

  TMeta tv -> do
    mb_ty <- readMetaTv tv
    case mv_ty of
      Nothing ->
        return $ TMeta tv
      
      Just ty -> do
        ty' <- zonkType ty
        writeMetaTv tv ty'
        return ty'


  TCon tc -> return $ TCon tc

  TApp l a m_scheme bs ->
    TApp l <$> zonkType a <*> pure m_scheme <*> fmap zonkType bs

  TArr l a b ->
    TArr l <$> zonkType a <*> zonkType b

  -- Primitive type constructors
  TPrimCon l ptc ->
    TPrimCon l <$> zonkPrimType ptc


  TOp l tops ->
    TOp l <$> fmap zonkTyOpTerm topsy

  TOpPrefix  l n ty ->
    TOpPrefix l n <$> zonkType ty 
  
  TOpPostfix l ty n ->
    TOpPostfix l <$> zonkType ty <*> pure n
  
  TOpInfix   l ty1 n ty2 ->
    TOpInfix l <$> zonkType ty1 <*> pure n <*> zonkType ty2
  
  TOpInfixL  l ty1 n ty2 ->
    TOpInfixL l <$> zonkType ty1 <*> pure n <*> zonkType ty2

  TOpInfixR  l ty1 n ty2 ->
    TOpInfixR l <$> zonkType ty1 <*> pure n <$> zonkType ty2


  TParens l ty ->
    TParens l <$> zonkType ty
  TKind l ty k ->
    TKind l <$> zonkType ty <*> pure k  -- Do kinds need zonking?


zonkPrimType :: Prim.Type Type Exp -> Tc (Prim.Type Type Exp)
zonkPrimType = \case
  TArray l ty -> TArray l <$> zonkType ty
  TArraySize l ty e -> TArraySized l <$> zonkType ty <*> zonkExp e
  TVector l ty -> TVector l <$> zonkType ty
  TVectorSized l ty e -> TVectorSize l <$> zonkType ty <*> zonkExp e
  ty -> return ty


zonkTyOpTerm :: TyOpTerm -> Tc TyOpTerm
zonkTyOpTerm = \case
  TyOperator n -> return $ TyOperator n
  TyOperand ty -> TyOperand <$> zonkType ty


zonkTypeSig :: TypeSig -> Tc TypeSig
zonkTypeSig (TypeSig m_l ty) = TypeSig m_l <$> zonkType ty

zonkExp :: Exp -> Tc Exp
zonkExp = \case
  EVar v    -> return $ EVar v
  EGlobal g -> return $ EGlobal g
  ECon c    -> return $ ECon c
  
  EValue l v -> EValue l <$> zonkPrimVal v
  EInstr l i -> EInstr l <$> zonkInstr i

  EOp l ops -> EOp l <$> fmap zonkOpTerm ops
  EOpPrefix l n e  -> EOpPrefix l n <$> zonkExp e
  EOpPostfix l e n -> EOpPostfix l <$> zonkExp e <*> pure n
  EOpInfix  l e1 n e2 -> EOpInfix  l <$> zonkExp e1 <*> pure n <*> zonkExp e2
  EOpInfixL l e1 n e2 -> EOpInfixL l <$> zonkExp e1 <*> pure n <*> zonkExp e2
  EOpInfixR l e1 n e2 -> EOpInfixR l <$> zonkExp e1 <*> pure n <*> zonkExp e2

  ECall l e args    -> ECall   l <$> zonkExp e <*> zonkArguments args
  EAssign l lhs rhs -> EAssign l <$> zonkExp lhs <*> zonkExp rhs

  EMember       l e n -> EMember       l <$> zonkExp e <*> pure n
  EPtrAccess    l e n -> EPtrAccess    l <$> zonkExp e <*> pure n
  EArrayAccess  l e i -> EArrayAccess  l <$> zonkExp e <*> zonkExp i
  EVectorAccess l e i -> EVectorAccess l <$> zonkExp e <*> zonkExp i

  ENew    l e -> ENew    l <$> zonkExp e
  ERenew  l e -> ERenew  l <$> zonkExp e
  EDelete l e -> EDelete l <$> zonkExp e

  EParens l e -> EParens l <$> zonkExp e
  EAs l e ty -> EAs l <$> zonkExp e <*> zonkExp ty
  EType m_l e sig -> EType m_l <$> zonkTypeSig sig


zonkOpTerm :: OpTerm -> Tc OpTerm
zonkOpTerm = \case
  Operand  e -> Operand <$> zonkExp e
  Operator n -> Operator n


zonkArguments :: Arguments -> Tc Arguments
zonkArguments (Arguments l es) = Arguments l <$> fmap zonkExp es


zonkPrimValue :: Prim.Value Exp -> Tc (Prim.Value Exp)
zonkPrimValue = \case
  Prim.VArray  es -> Prim.VArray  <$> fmap zonkExp es
  Prim.VVector es -> Prim.VVector <$> fmap zonkExp es
  v -> return v

zonkInstr :: Prim.Instruction Exp -> Tc (Pim.Instruction Exp)
zonkInstr = \case
  Prim.IAdd a b -> Prim.IAdd <$> zonkExp a <*> zonkExp b
  Prim.ISub a b -> Prim.ISub <$> zonkExp a <*> zonkExp b
  Prim.IMul a b -> Prim.IMul <$> zonkExp a <*> zonkExp b
  Prim.IDiv a b -> Prim.IDiv <$> zonkExp a <*> zonkExp b
  Prim.UDiv a b -> Prim.UDiv <$> zonkExp a <*> zonkExp b
  Prim.SDiv a b -> Prim.SDiv <$> zonkExp a <*> zonkExp b

  -- Floating Point Math
  Prim.FAdd a b -> Prim.FAdd <$> zonkExp a <*> zonkExp b
  Prim.FSub a b -> Prim.FSub <$> zonkExp a <*> zonkExp b
  Prim.FMul a b -> Prim.FMul <$> zonkExp a <*> zonkExp b
  Prim.FDiv a b -> Prim.FDiv <$> zonkExp a <*> zonkExp b

  -- Comparisions
  Prim.Eq    a b -> Prim.Eq    <$> zonkExp a <*> zonkExp b
  Prim.Lt    a b -> Prim.Lt    <$> zonkExp a <*> zonkExp b
  Prim.LtEq  a b -> Prim.LtEq  <$> zonkExp a <*> zonkExp b
  Prim.Gt    a b -> Prim.Gt    <$> zonkExp a <*> zonkExp b
  Prim.GtEq  a b -> Prim.GtEq  <$> zonkExp a <*> zonkExp b
  Prim.NEq   a b -> Prim.NEq   <$> zonkExp a <*> zonkExp b
  Prim.NLt   a b -> Prim.NLt   <$> zonkExp a <*> zonkExp b
  Prim.NLtEq a b -> Prim.NLtEq <$> zonkExp a <*> zonkExp b
  Prim.NGt   a b -> Prim.NGt   <$> zonkExp a <*> zonkExp b
  Prim.NGtEq a b -> Prim.NGtEq <$> zonkExp a <*> zonkExp b

