; ModuleID = 'test'
source_filename = "<string>"

declare i32 @printf(i8*, ...)

define i32 @main() {
entry:
  %0 = add i32 5, 5
  %x = alloca i32, align 1
  store i32 %0, i32* %x, align 1
  %1 = alloca [16 x i8], align 1
  %2 = alloca i8*, align 1
  store [16 x i8] c"Hello World!\0A%d\0A", [16 x i8]* %1, align 1
  %3 = bitcast [16 x i8]* %1 to i8*
  store i8* %3, i8** %2, align 1
  %4 = load i8*, i8** %2, align 1
  %str = alloca i8*, align 1
  store i8* %4, i8** %str, align 1
  %5 = load i8*, i8** %str, align 1
  %6 = load i32, i32* %x, align 1
  %7 = call i32 (i8*, ...) @printf(i8* %5, i32 %6)
  %8 = load i32, i32* %x, align 1
  ret i32 %8
}
